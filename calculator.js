const num1 = document.querySelector('#num1');
const num2 = document.querySelector('#num2');
const btnSumar = document.querySelector('.sumar');
const btnRestar = document.querySelector('.restar');
const btnMultiplicar = document.querySelector('.multiplicar');
const btnDividir = document.querySelector('.dividir');
const output = document.querySelector('output');

const calculadora = {
    numeros: [0, 0],
    sumar: function () {
        return this.numeros[0] + this.numeros[1];
    },
    restar: function () {
        return this.numeros[0] < this.numeros[1] ? this.numeros[0] - this.numeros[1] : this.numeros[1] - this.numeros[0];
    },
    dividir: function () {
        return this.numeros[0] < this.numeros[1] ? this.numeros[0] / this.numeros[1] : this.numeros[1] / this.numeros[0];
    },
    multiplicar: function () {
        return this.numeros[0] * this.numeros[1];
    },
    setNumeros: function (n1, n2) {
        return this.numeros = [Number(n1), Number(n2)];
    }
}

function sumarHandler (e) {
    let suma = Object.create(calculadora);
    suma.setNumeros(num1.value, num2.value);
    output.textContent = suma.sumar();
}

function restarHandler (e) {
    let restar = Object.create(calculadora);
    restar.setNumeros(num1.value, num2.value);
    output.textContent = restar.restar();
}

function multiplicarHandler (e) {
    let multiplicar = Object.create(calculadora);
    multiplicar.setNumeros(num1.value, num2.value);
    output.textContent = multiplicar.multiplicar();
}

function dividirHandler (e) {
    let dividir = Object.create(calculadora);
    dividir.setNumeros(num1.value, num2.value);
    output.textContent = dividir.dividir();
}

btnSumar.addEventListener('click', sumarHandler);
btnRestar.addEventListener('click', restarHandler);
btnMultiplicar.addEventListener('click', multiplicarHandler);
btnDividir.addEventListener('click', dividirHandler);